<?php

trait Hewan {
	public $nama;
	public $darah = 50;
	public $jumlahKaki;
	public $keahlian;

	public function atraksi(){
		echo $this->nama.' sedang '.$this->keahlian.'<br><br>';
	}
}

trait Fight {
	public $attackPower;
	public $deffensePower;

	function serang($lawan){
		$lawan->darah = $lawan->darah - ($this->attackPower/$lawan->deffensePower);

		if($lawan->darah <=0){
			echo $this->nama.' sedang menyerang '.$lawan->nama.'<br>';
			echo $lawan->nama.' mati <br><br>';
		} else {
			echo $this->nama.' sedang menyerang '.$lawan->nama.'<br>';
			echo 'Sisa darah '.$lawan->nama.' : '.$lawan->darah.'<br><br>';
		}
		
	}

	function diserang($lawan){
		$this->darah = $this->darah - ($lawan->attackPower/$this->deffensePower);

		if($this->darah <=0){
			echo $this->nama.' sedang diserang '.$lawan->nama.'<br>';
			echo $this->nama.' mati <br><br>';
		} else {
			echo $this->nama.' sedang diserang '.$lawan->nama.'<br>';
			echo 'Sisa darah '.$this->nama.' : '.$this->darah.'<br><br>';
		}
	}
}

class Elang {
	use Hewan, Fight {
        // Hewan::__construct insteadof Fight;
    }

    function __construct($nama){
    	$this->nama = $nama;
	    $this->jumlahKaki = 2;
	    $this->keahlian = 'terbang tinggi';
	    $this->attackPower = 10;
	    $this->deffensePower = 5;
    }
    

	function getInfoHewan(){
		echo 'Jenis : Elang. <br>';
		echo 'Nama : '.$this->nama.'. <br>';
		echo 'Darah : '.$this->darah.'. <br>';
		echo 'Jumlah Kaki : '.$this->jumlahKaki.'. <br>';
		echo 'Keahlian : '.$this->keahlian.'. <br>';
		echo 'Kekuatan Serang : '.$this->attackPower.'. <br>';
		echo 'Kekuatan Bertahan : '.$this->deffensePower.'. <br><br>';
	}
}

class Harimau {
	use Hewan, Fight {
        // Hewan::__construct insteadof Fight;
    }

    function __construct($nama){
    	$this->nama = $nama;
	    $this->jumlahKaki = 4;
	    $this->keahlian = 'lari cepat';
	    $this->attackPower = 7;
	    $this->deffensePower = 8;
    }

	function getInfoHewan(){
		echo 'Jenis : Harimau. <br>';
		echo 'Nama : '.$this->nama.'. <br>';
		echo 'Darah : '.$this->darah.'. <br>';
		echo 'Jumlah Kaki : '.$this->jumlahKaki.'. <br>';
		echo 'Keahlian : '.$this->keahlian.'. <br>';
		echo 'Kekuatan Serang : '.$this->attackPower.'. <br>';
		echo 'Kekuatan Bertahan : '.$this->deffensePower.'. <br><br>';
	}
}

$harimau_jawa = new Harimau('Harimau Jawa');
$harimau_jawa->getInfoHewan();

$elang_jawa = new Elang('Elang Jawa');
$elang_jawa->getInfoHewan();

$elang_gundul = new Elang('Elang Gundul');
$elang_gundul->getInfoHewan();

$harimau_jawa->atraksi();
$elang_jawa->atraksi();

$elang_jawa->serang($harimau_jawa);

$elang_jawa->diserang($harimau_jawa);

while($elang_jawa->darah>0){
	$harimau_jawa->serang($elang_jawa);
}

while($harimau_jawa->darah>0){
	$harimau_jawa->diserang($elang_gundul);
}

?>